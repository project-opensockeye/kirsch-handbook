.. Kirsch Handbook documentation master file, created by
   sphinx-quickstart on Fri Mar  1 14:31:02 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Kirsch Handbook!
===========================================

This is the root of the Kirsch handbook.  To get started:

- get familiar with `reStructuredText`_ or `MarkDown`_
- write in ``tutorials/`` already
- if you add a new subfolder, record it down in the ``toctree`` directive (like ``design``)
- refer to the `Sphinx documentation`_ for more information

.. toctree::
   :maxdepth: 2
   :caption: Development 
   :glob:

   development/*

.. toctree::
   :maxdepth: 2
   :caption: Design
   :glob:

   design/*

.. toctree::
   :maxdepth: 2
   :caption: Tutorials
   :glob:

   tutorials/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`

.. _`reStructuredText`: https://docutils.sourceforge.io/docs/user/rst/quickref.html
.. _`MarkDown`: https://www.markdownguide.org/cheat-sheet/
.. _`Sphinx documentation`: https://www.sphinx-doc.org/en/master/usage/index.html
