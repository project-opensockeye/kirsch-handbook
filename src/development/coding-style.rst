Kirsch Coding Style
===================

Documentation Comments
----------------------

Document functions/classes/assembly macros with comments.
The format is intended to be understandable by `Doxygen <https://www.doxygen.nl/manual/docblocks.html>`_ and IDEs (therefore starts with `/**`, two asterisks)

.. code-block:: c

        /**
        * Brief description. Do not repeat the function signature.
        * @param param1 Description.
        * @param param2 Description
        *               that wraps to the next line.
        * @return Return value.
        * @note Something to know.
        */
 

