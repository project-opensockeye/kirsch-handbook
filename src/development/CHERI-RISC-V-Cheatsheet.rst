CHERI RISC-V Cheatsheet
=======================

The following is a Cheatsheet designed to ease development with CHERI RISC-V.

The Cheatsheet is best-effort and may be incomplete.
If you notice a mistake or think a piece of information is missing, open a merge request with your proposal!

Registers
---------

.. list-table:: Registers 
  :header-rows: 1

  * - Register
    - ABI Name
    - Description 
    - Purecap Saver
  * - c0
    - cnull
    - NULL Pointer  
    - 
  * - c1
    - cra
    - Return address 
    - Caller
  * - c2
    - csp
    - Stack Pointer 
    - Callee 
  * - c3
    - cgp
    - Global Pointer 
    - 
  * - c4
    - ctp
    - Thread Pointer 
    - 
  * - c5
    - ct0
    - Temporary/alternate link resgister 
    - Caller
  * - c6-7
    - ct1-2
    - Temporaries 
    - Caller
  * - c8
    - cs0/cfp
    - Saved regsister/frame pointer 
    - Callee 
  * - c9
    - cs1
    - Saved register 
    - Callee 
  * - c110-11
    - ca0-1
    - Function arguments/return values 
    - Caller
  * - c12-17
    - ca2-7
    - Function arguments 
    - Caller
  * - c18-27
    - cs2-11
    - Saved Registers 
    - Callee 
  * - c28-31
    - ct3-6       
    - Temporaries 
    - Caller


Capability Instructions
-----------------------

Capability length in imm: 16

.. list-table:: Capability Instructions 
  :header-rows: 1

  * - (Pseudo) Instruction 
    - Semantics 
    - Comment 
  * - auipcc cd, imm              
    - cd <= PCC with PCC.addr + (imm << 12)    
    - If imm is %\[captab\_\]pcrel_hi(sym), equivalent to load high 20 bits of abs addr sym to cd                     
  * - cincoffset cd, cs1, rs2/imm 
    - cd <= cs1 with addr += rs2/imm           
    -                                                                                                                
  * - cllc cd, sym                
    - cd <= PCC with addr to sym               
    - 1: auipcc cd, %pcrel_hi(sym)<br>cincoffset cd, cd, %pcrel_lo(1b)                                               
  * - clc cd, imm(cs1)            
    - cd <= Mem[cs1.addr + imm]                
    - Exception if cs1 sealed, no Permit_Load, cd invalidated **without** exception if cs1 no Permit_Load_Capability 
  * - clgc cd, sym                
    - cd <= CapTable\[sym\]                    
    - 1: auipcc cd, %captab_pcrel_hi(sym)<br>clc cd, %pcrel_lo(1b)(cd)                                               
  * - csc cs2, imm(cs1)           
    - Mem[cs1.addr + imm] <= cs2               
    - Exception if cs1 unseal or no Permit_Store((_Local)_Capability)                                                
  * - csetaddr cd, cs1, cs2       
    - cd <= cs1 with cs2.addr                  
    - Exception if cs1 is sealed                                                                                     
  * - csetbounds cd, cs1, rs2     
    - cd <= cs1.addr as base and rs2 as length 
    - Exception if cs1 is sealed or has smaller length                                                               
  * - cjalr \[cd,\] cs1           
    - cd <= next inst, PCC <= cs1              
    - cd is cra if omitted, cd auto sealed, cs1 auto unsealed                                                        
  * - cret                        
    - cjalr cnull, cra                         
    -                                                                                                                
  * - cseal cd, cs1, cs2          
    - cd <= seal cs1 with cs2.addr             
    -                                                                                                                
  * - cmove cd, cs1               
    - cd <= cs1                                
    -                                                                                                                

Capability-Extended Data Instructions
-------------------------------------

.. list-table:: Capability-Extended Data Instructions 
  :header-rows: 1

  * - (Pseudo) Instruction     
    - Semantics                          
    - Comment
  * - cl{bhwd} rd, offset(cs)  
    - rd <- Mem[cs + offset] as signed   
    -         
  * - cl{bhwd}u rd, offset(cs) 
    - rd <- Mem[cs + offset] as unsigned 
    -         
  * - cs{bhwd} rs, offset(cds) 
    - Mem[cd + offset] <- rs             
    -         
