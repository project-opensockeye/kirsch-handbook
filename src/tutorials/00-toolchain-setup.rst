.. _pre-built-image:
.. _toolchain-setup:

Acquiring the Kirsch Toolchain
==============================

We provide a docker image that contains all the necessary tools for building
and running Kirsch. Thus, you only need to ensure that you have `installed
Docker <https://docs.docker.com/engine/install/>`_.

.. _docker-setup:

Setting up Docker
^^^^^^^^^^^^^^^^^

The first step is to ensure that your user is able to run docker images.
The exact requirements for this differ by operating system:

Ubuntu
  Ensure that your user is added to the ``docker`` group on your system.

MacOS
  Should work out-of-the-box after installing Docker

Pulling the image
^^^^^^^^^^^^^^^^^

You can pull the docker image from ETH's image registry using the following
command::

    docker pull registry.ethz.ch/project-opensockeye/kirsch-toolchain

Note that the image is quite large (a few gigabytes), so this step might take a
while.

Running the image
^^^^^^^^^^^^^^^^^

Once you have successfully pulled the image, you should be able to run it::

   docker run --rm -v $(pwd):/src -it registry.ethz.ch/project-opensockeye/kirsch-toolchain bash

This command will run an interactive ``bash`` shell inside a newly created
container, and mount your current working directory to ``/src``. Here's a brief
breakdown of the docker command::

   docker run         # Runs a docker image

     --rm             # Delete the container state after exiting the container
                      # By default, a terminated image sticks around until it
                      # is cleaned up, which wastes space

     -v $(pwd):/src   # Mount your current working directory to /src.
                      # The general syntax for mounting volumes is
                      # <local path>:<in-container path>. This will ensure
                      # that any files stored in either location correctly shows
                      # up on your local filesystem.

     -it              # Flags for interactive operation

     registry.ethz.ch/project-opensockeye/kirsch-toolchain
                      # The image to be run

     bash             # The command to be run

Tools included
^^^^^^^^^^^^^^

The docker image comes with a bunch of useful tools installed:

-  `Sockeye3 <https://gitlab.inf.ethz.ch/project-opensockeye/sockeye3>`_, our
   Rust implementation of decoding nets with associated SoC specifications.

-  LLVM built with RISC-V CHERI support

   -  We rely on the `fork that CTSRD group maintains <https://github.com/CTSRD-CHERI/llvm-project>`__.

-  Simulators for Arm Morello and RISC-V CHERI

   - Two RISC-V CHERI simulators are provided, an Ocaml-based one and a C-based
     one. Both of these are built from the `sail-cheri-riscv
     <https://github.com/CTSRD-CHERI/sail-cheri-riscv>`__ project.

   - In addition, we provide Arm FastModels and a Morello FVP.

-  The RISC-V tests library

   -  ``cheriette`` contains some headers that are provided by the riscv-tests
      library.

The exact versions of the tools provided can be seen in the `Dockerfile
<https://gitlab.inf.ethz.ch/project-opensockeye/kirsch-toolchain/-/blob/main/Dockerfile>`__.

File System Layout
^^^^^^^^^^^^^^^^^^

As is usual for Docker-based setups, the file system hierarchy is close to the
file system root. The following folders are important

-  ``/src``: Contains the mounted Kirsch source code
-  ``/sim``: Contains the RISC-V simulators
-  ``/tools/``: Contains additional tools: sockeye3, the riscv-tests library,
   an LLVM fork with CHERI support, the Arm Morello FVP, etc.

Building the Docker Container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
   We provide a pre-built image for your convenience. See :ref:`this section
   <pre-built-image>` for instructions how to use it.

The docker image can be manually built `from this repo
<https://gitlab.inf.ethz.ch/project-opensockeye/kirsch-toolchain>`__. In order
to build the docker image, you need to make sure that your user :ref:`can run
docker <docker-setup>`.

The ARM FVPs for a standard installation are downloaded automatically by the
Docker container.

Files included in a standard installation:

- ``./installers/``, either the amd64 or arm64 versions of: 

    - ``./installers/fvp/``, the Corstone and Morello FVPs, which are freely
      available and do not require a license

        - Corstone 500
        - Corstone 1000
        - Corstone SSE-310
        - Corstone SSE-315
        - Morello (amd64 only)

    - ``./installers/fastmodels/``, FastModels that require no license to use

        - None

An extended installation may additionally install the following FastModels:

- ``./installers/fastmodels/``, FastModels that require a license to use

   - FastModels 11-25_15
   - FastModels ThirdParty IP 11-25_15

You can build the docker container using the following build command (note the
trailing ``.``)::

   ARCH=amd64  # or arm64 for 64-bit Arm targets, e.g. M1 Macs
   docker build -t kirsch-toolchain --build-arg arch=$ARCH .

Alternatively, you can use ``docker buildx``, which is the recommended
replacement for ``docker build`` (again, note the trailing ``.``)::

   docker buildx build --platform linux/$ARCH -t kirsch-toolchain .

Either command will build a docker image containing all relevant tools. This
image will be called ``kirsch-toolchain`` and tagged with the default tag
``latest``\ [1]_.

.. [1]
   This is mostly relevant if you need to build multiple images, e.g. for
   testing upgrades.
