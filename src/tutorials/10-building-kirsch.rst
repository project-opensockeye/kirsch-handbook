
Building Kirsch
===============

Kirsch uses a custom, python-based build system to generate a Ninja build file.
Ninja then resolves the generated build file and runs all the compilation commands.

To generate the Ninja build file, run the following:
 - In an environment with the complete Toolchain
 - From the root of the Kirsch git repository 

.. code-block:: bash 

    python3 generators/build.py > build/ninja.build

Then run Ninja in the ``build`` directory

.. code-block:: bash

    ninja -C build

At time of writing, this will generate artifacts for ``riscv64`` and ``morello`` in ``build/``:
 - ``kernel_ARCH.elf``: The Kirsch kernel for architecture ARCH as an ELF executable.
 - ``loader_ARCH.elf``: The Kirsch Loader with the kernel ELF embedded inside, as an ELF executable.
 - ``loader_ARCH.bin``: The Kirsch laoder with the kernel ELF embedded inside, as a binary blob. The first byte of ``loader_ARCH.bin`` is the first instruction to execute the loader.

In addition, the Ninja Morello target will compile Morello ARM ATF binaries 
by executing the ``generators/morello_atf_package.sh`` script.

If you'd like to only build the artifacts of a single architecture, use the target
``$ARCH_all``, e.g. ``morello_all`` or ``riscv64_all``.

To clean all build files, use

.. code-block:: bash

    ninja -C build -t clean
