
Executing Kirsch
================

Kirsch supports the following execution targets:
 - ARM Morello FVP
 - CHERI RISC-V QEMU

The appropriate software is pre-installed in the :ref:`Kirsch toolchain<toolchain-setup>`,
and this guide assumes execution in the Kirsch toolchain container.

ARM Morello FVP
---------------

First, build the Morello loader ``morello_all`` target.

.. code-block:: bash
    
    ninja -C build morello_all

To launch the Morello FVP with UART output to console, run:

.. code-block:: bash

    bash scripts/fvp-run.sh

or

.. code-block:: bash

    bash scripts/fvp-run-console.sh

If you have opened an X11 session, you can open the FVP graphical view with:

.. code-block:: bash

    bash scripts/fvp-run-x11.sh

CHERI RISC-V QEMU
-----------------

First, build the CHERI RISC-V ``riscv64_all`` target.

.. code-block:: bash
    
    ninja -C build riscv64_all

To launch QEMU with console output, run:

.. code-block:: bash

    bash scripts/qemu_run.sh
