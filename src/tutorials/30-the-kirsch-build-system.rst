
Build system
=======================

The Kirsch build system consits of a two-step process:

 1. The python script `generators/build.py` is used to generate a Ninja build file.
 2. Ninja executes the Ninja build file.

Principles
----------

The user defines ``artifacts`` and ``contexts``.

An ``artifact`` is a file or set of files created during the Ninja build step,
for example, an ELF file or a static library. 

The base-artifact is located in ``generators/build/lib/artifacts.py``, alongside 
base artifacts:
 
 - ``ArtifactCompiled`` is an artifact that represents a set of source files 
   that are compiled into object files.
 - ``ArtifactElf`` takes the compiled ouptut of ``ArtifactCompiled`` and 
   links it into an ELF binary file.
 - ``ArtifactStaticLib`` takes the compiled ouptut of ``ArtifactCompiled`` and 
   archives it into a static library.
 - ``ArtifactBinary`` takes an ELF file and 
   a second payload file, inserts the payload file into the ELF file, and
   then strips the ELF header of the final ELF.
   This is used for creating Kirsch loader binaries.

A ``context`` defines various parameters that can be passed to, and used by, 
artifacts.
A ``context`` can include:
 
 - ``includes``, a list of directories to pass to compilers and linkers.
 - ``includes_arch``, a dictionary defining architecture-dependent 
   includes.
 - ``cflags``, a list of flags to pass to compilers.
 - ``cflags_arch``, a dictionary defining architecture-dependent 
   compiler flags.
 - ``ldflags``, a list of flags to pass to linkers.
 - ``ldflags_arch``, a dictionary defining architecture-dependent 
   linker flags.
 - ``arflags``, a list of flags to pass to ``ar``, the tool used to 
   create static libraries.
 - ``arflags_arch``, a dictionary defining architecture-dependent 
   ``ar`` flags.
 - ``arflags``, a list of flags to pass to ``objcopy``, used when creating 
   loader binaries..
 - ``arflags_arch``, a dictionary defining architecture-dependent 
   ``objcopy`` flags.

A ``context`` can then be optionally stripped of either its defines flags or 
includes, and passed to the ``artifact``. 
 

How to add a Context
--------------------

Contexts are defined in ``generators/build/contexts.py``.

Add the new context at the end of the ``contexts`` dictionary in the ``get()``
function.
The ``str``-type key is should be the same as the name of the context you define.

How to add a new artifacts
--------------------------

Every artifact is defined in its own file in ``generators/build/artifacts``.
In ``generators/build.py`` the artifact definition from ``generators/build/artifacts``
is then instantiated and combined with other artifacts.
