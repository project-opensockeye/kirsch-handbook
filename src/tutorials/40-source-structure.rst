
Souce Structure
===============

This is a rough description of the source tree.
Please note that the structure is not finalized, and will change.

If you do make changes to the structure, make them 
in such a way that they are well documented and thoroughly executed. 

 - `build/` is the directory for *all* ephemeral build artifacts, with the
    exception of generated headers. All other derived and generated files end up here. 
 - `compat/` includes wholesale copies of other projects, e.g. Free/CHERIBSD headers.
   Keeping this code separate signals that it is not ''normal'' Kirsch code and 
   enables easier updating/replacing of the entire borrowed source tree.
 - `drivers/` unsure if this should be included...?
 - `generators/` Tools and their configuration for generating source or build files.
   In particular, this is where the python-based Ninja-build file generator.
 - `include/` Headers that are not part of a particular library or part of `compat/`.
 - `kernel/` Kernel sources.
 - `lib/` 
   - `foreign/` non-Kirsch libraries, similar in function to `compat`, but 
     for artifacts that need to be compiled.
   - `kirsch/` Kirsch libraries.
 - `link/` Until linker scripts are generated by the build system, they are temporarily
   all placed here.
 - `loader/` Home of all loader sources.
 - `mackerel/` Mackerel device specifications and support headers.
 - `scripts/` Various scripts for running emulators and formatting source code.
 - `usr/` Userspace sources.
