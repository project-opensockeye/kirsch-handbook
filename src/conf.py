# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Kirsch Handbook'
copyright = '2024, Systems Group @ ETH Zürich'
author = 'Systems Group @ ETH Zürich'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'README.md']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_favicon = '_static/favicon.png'

# Set by the Gitlab runner, left blank otherwise
html_context = {
    'commit': os.getenv('CI_COMMIT_SHORT_SHA', '')
}

# -- LaTeX fonts -------------------------------------------------------------

latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '11pt',
    'preamble': r'''
        \usepackage{charter}
        \usepackage[defaultsans]{lato}
        \usepackage{inconsolata}
    ''',
}

# -- Other options -----------------------------------------------------------
# Apparently, this is the way to add custom lexers to sphinx.
from pygments.lexer import RegexLexer
from pygments import token
from sphinx.highlighting import lexers

class SockeyeLexer(RegexLexer):
    name = 'sockeye'

    tokens = {
        'root': [
            (r'\/\/\/\/.*', token.Comment),
            (r'\/\/\/.*', token.Literal.String),
            (r'\/\/.*', token.Comment),
            (r'\bas\b', token.Keyword),
            (r'\bin\b', token.Keyword),
            (r'\bmem\b', token.Keyword),
            (r'\bdev\b', token.Keyword),
            (r'\bres\b', token.Keyword),
            (r'\bctx\b', token.Keyword),
            (r'\bmap\b', token.Keyword),
            (r'\bout\b', token.Keyword),
            (r'\binst\b', token.Keyword),
            (r'\bimport\b', token.Keyword),
            (r'\bconn\b', token.Keyword),
            (r'@', token.Operator),
            (r'->', token.Operator),
            (r',', token.Punctuation),
            (r'\(', token.Punctuation),
            (r'\)', token.Punctuation),
            (r'(B|kiB|MiB|GiB|TiB|PiB|EiB)', token.Literal.Number),
            (r'0x[0-9A-Fa-f_]+', token.Literal.Number),
            (r'0x[0-9A-Fa-f_]+', token.Literal.Number),
            (r'[a-zA-Z_][_a-zA-Z.0-9/]*', token.Name),
            (r'[0-9][0-9_]*', token.Literal.Number),
            (r'\s', token.Text)
        ]
    }

lexers['sockeye'] = SockeyeLexer(startinline=True)
