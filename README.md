# Kirsch Handbook

To build the documentation tree, setup the virtualenv first:

```console
$ python3 -m virtualenv kirsch-doc
$ source kirsch-doc/bin/activate
$ cd <doc root>
$ pip install -r requirements.txt
```

Then build with `make html` or `make latex`.  To host a local version of the built HTML tree:

```console
$ cd _build/html
$ python -m http.server
```

To run water + html server that automatically update the output on file changes:

```console
$ pip3 install sphinx-autobuild
$ make auto_html
```

If you've installed any Python packages for your magic, remember to update `requirements.txt`:

```console
$ pip-chill > requirements.txt
```
